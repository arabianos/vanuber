package com.vanuber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VanuberApplication {

	public static void main(String[] args) {
		SpringApplication.run(VanuberApplication.class, args);
	}

}

